#include<stdio.h>
void hanio (int n,char a,char b,char c);
int main (void)
{
    int n;
    char a,b,c;
    scanf("%d",&n);
    scanf("%c %c %c",&a,&b,&c);
    hanio(n,a,b,c);
    
    return 0;
}
void hanio (int n,char a,char b,char c)
{
    if(n==1)
      printf("%d: %c -> %c\n",n,a,b);
    else {
        hanio (n-1,a,c,b);
        printf("%d: %c -> %c\n",n,a,b);
        hanio(n-1,c,b,a);
    }
}